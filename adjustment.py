import numpy as np

def adjust(df, dfs,code,years, diagram,key,country):
    
    value_minor1=value_minor2=value_minor3=0
    
    value_major = df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]]
    if not value_major.isnull().values.all(): 
        value_major = float(value_major.to_string(index=False, header=False))
        if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if  not df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                value_minor1=df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]]
                value_minor1 = float(value_minor1.to_string(index=False, header=False))
            else :
                value_minor1=0
            
                    
        if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if  not df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                value_minor2=df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]]
                value_minor2 = float(value_minor2.to_string(index=False, header=False))
            else :
                value_minor2=0
            
                
        if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if  not df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                value_minor3=df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]]
                value_minor3 = float(value_minor3.to_string(index=False, header=False))
            else :
                value_minor3=0
            
        if not (value_minor1+value_minor2+value_minor3)==0 :   

            value_minor1= value_minor1*value_major/(value_minor1+value_minor2+value_minor3)
            value_minor2= value_minor2*value_major/(value_minor1+value_minor2+value_minor3)
            value_minor3= value_minor3*value_major/(value_minor1+value_minor2+value_minor3)
            #print('6601=',value_minor1,'6680=',value_minor2,'6773=',value_minor3)
                
            df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]=value_minor1
            df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2
            df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]=value_minor3
    
    return df