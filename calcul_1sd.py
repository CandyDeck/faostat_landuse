import yaml

with open(r'small_diagrams.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    diagram = yaml.load(file, Loader=yaml.FullLoader)
    
def calculation(df,code,key,years):
    if not df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]].isnull().values.all():
        value_major = df.loc[(df['Item Code']==key)&(df['ISO3']==code),[years]]
        value_major=float(value_major.to_string(index=False, header=False))
    
        if ((diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor3 = df.loc[(df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]] = value_major - value_minor3
                            
        if ((diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor2 = df.loc[(df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code),[years]]
                        value_minor2 = float(value_minor2.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]] = value_major - value_minor2                          

        if ((diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor3 = df.loc[(df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]] = value_major - value_minor3
                            
        if ((diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor1 = df.loc[(df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code),[years]]
                        value_minor1 = float(value_minor1.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]] = value_major - value_minor1  
                                    
                                    
        if ((diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor2 = df.loc[(df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code),[years]]
                        value_minor2 = float(value_minor2.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]] = value_major - value_minor2
                                
        if ((diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) ):
            if not diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor1 = df.loc[(df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code),[years]]
                        value_minor1 = float(value_minor1.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]] = value_major - value_minor1                                      
                                    
                                    
        if ((diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values)):
            if df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                if not df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    value_minor2 = df.loc[(df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code),[years]]
                    value_minor2 = float(value_minor2.to_string(index=False, header=False))
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor3 = df.loc[(df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]] = value_major - value_minor2 - value_minor3                                   
                                    
        if ((diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values)):
            if df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                if not df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    value_minor1 = df.loc[(df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code),[years]]
                    value_minor1 = float(value_minor1.to_string(index=False, header=False))
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor3 = df.loc[(df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]] = value_major - value_minor1 - value_minor3                                   

        if ((diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values) and  (diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values)):
            if df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                if not df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                    value_minor1 = df.loc[(df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code),[years]]
                    value_minor1 = float(value_minor1.to_string(index=False, header=False))
                    if not df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        value_minor2 = df.loc[(df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code),[years]]
                        value_minor2 = float(value_minor2.to_string(index=False, header=False))
                        df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]] = value_major - value_minor1 - value_minor2
                    
    return df