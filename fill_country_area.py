import yaml

with open(r'parameters.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    parameters = yaml.load(file, Loader=yaml.FullLoader)
 
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)



def fill(df, code,col_years):
    relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    if code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
    if not (df.loc[(df['ISO3']==code)&(df['Item Code']==6600)&(df['Unit']=='1000 ha'),col_years].isnull().values.all()) :
        diff_value_per_line = df.loc[(df['ISO3']==code)&(df['Item Code']==6600),col_years].nunique(axis=1,dropna=True)
        diff_value_per_line=int(diff_value_per_line)
        nber_value_per_line = df.loc[(df['ISO3']==code)&(df['Item Code']==6600),col_years].count(axis=1, numeric_only=True)
        nber_value_per_line=nber_value_per_line.astype(int)
        if  (diff_value_per_line == 1):
            value = df.loc[(df['ISO3']==code)&(df['Item Code']==6600),col_years].mean(axis=1,skipna=True) 
            value = value.to_string(index=False, header=False)
            for years in relevant_years :
                if (df.loc[(df['ISO3']==code)&(df['Item Code']==6600),[years]].isnull().values.any()):
                    df.loc[(df['ISO3']==code)&(df['Item Code']==6600),[years]]=value
    return df