import yaml

with open(r'diagram.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    diagram = yaml.load(file, Loader=yaml.FullLoader)
    
def pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust):
    for years in relevant_years_adjust2:
        value_major = df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]]
        if not value_major.isnull().values.all():  
            value_major = float(value_major.to_string(index=False, header=False))
            if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all()):    
                    std_minor1 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),'std_dev'].item()  
                    print(std_minor1)
                    if std_minor1 <=5 :
                        mean_minor1 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),'mean_value'].item() 
                        value_minor1 = value_major*mean_minor1/100
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]=value_minor1
            if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :            
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all()):    
                    std_minor2 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor2")),'std_dev'].item()  
                    if std_minor2 <=5 :
                        mean_minor2 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor2")),'mean_value'].item() 
                        value_minor2 = value_major*mean_minor2/100
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2
            if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all()):    
                    std_minor3 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),'std_dev'].item()  
                    if std_minor3 <=5 :
                        mean_minor3 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),'mean_value'].item() 
                        value_minor3 = value_major*mean_minor3/100
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]=value_minor3
                
            if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) : 
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all()):     
                    if not ((df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all())&(df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all())):
                        value_minor1=df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]
                        value_minor1 = float(value_minor1.to_string(index=False, header=False)) 
                        value_minor3=df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        value_minor2=value_major-value_minor1-value_minor3
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                    if not (df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                        value_minor3=df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        value_minor2=value_major-value_minor3
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                    if not (df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                        value_minor1=df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]
                        value_minor1 = float(value_minor1.to_string(index=False, header=False))
                        value_minor2=value_major-value_minor1
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2                 
    
    for years in relevant_years_adjust:
        value_major = df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]]
        if not value_major.isnull().values.all(): 
            value_major = float(value_major.to_string(index=False, header=False))
            if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all()):    
                    std_minor3 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),'std_dev'].item()  
                    if std_minor3 <=5 :
                        mean_minor3 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),'mean_value'].item() 
                        value_minor3 = value_major*mean_minor3/100
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]=value_minor3
            if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :            
                if (df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all()):    
                    std_minor1 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),'std_dev'].item()  
                    if std_minor1 <=5 :
                        mean_minor1 = dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),'mean_value'].item() 
                        value_minor1 = value_major*mean_minor1/100
                        df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]=value_minor1
 
    return df