import yaml
import calcul_1 as cal1
import calcul_2 as cal2
import adjustment as adj
import calcul_3_case4_minor1 as cal3c4m1
import calcul_3_case4_minor2 as cal3c4m2
import calcul_3_case4_minor3 as cal3c4m3
import make_table_pourcentage_case4_minor1 as mtpc4m1
import make_table_pourcentage_case4_minor2 as mtpc4m2
import make_table_pourcentage_case4_minor3 as mtpc4m3
import empty_cells as ec
import calcul_pourcent_case2_minor3 as cpc2m3
import calcul_pourcent_case2_minor2 as cpc2m2
import calcul_pourcent_case2_minor1 as cpc2m1
import calcul_pourcent_all as cpa


with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)
    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)

def solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a):

    if year3b and (min(a)==year3b) and year3b<parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

        for years in relevant_years_adjust :             
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)
        mtpc4m3.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False) 
       
        
    if year1b and (min(a)==year1b) and year1b<parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)     
        
        mtpc4m1.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False)  
  
    if year2b and (min(a)==year2b) and year2b<parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)
               
        mtpc4m2.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False)                 
    df.to_csv('land_use.csv',index = False)
    
    
    missing = ec.check(df, code, relevant_years,key,missing)

            
    if missing == 1 :
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
        for years in relevant_years : 
            cpa.pourcentage(df, dfs,code,years, diagram,key,country)   
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
    
    if year3b and (min(a)==year3b) and year3b>parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year3b,parameters.get("exeptions").get(code).get("end")+1))]
        for years in relevant_years_adjust :             
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)
        relevant_years_adjust2= [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year3b))]
        for years in relevant_years_adjust2:
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c4m3.calculation(df,code,key,years)
        mtpc4m3.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False) 
       
        
    if year1b and (min(a)==year1b) and year1b>parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year1b,parameters.get("exeptions").get(code).get("end")+1))]
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)     
        relevant_years_adjust2= [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year1b))]
        for years in relevant_years_adjust2:
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c4m1.calculation(df,code,key,years)
        mtpc4m1.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False)  
  
    if year2b and (min(a)==year2b) and year2b>parameters.get("exeptions").get(code).get("begin"):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year2b,parameters.get("exeptions").get(code).get("end")+1))]      
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)
        relevant_years_adjust2= [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year2b))]
        for years in relevant_years_adjust2:
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c4m2.calculation(df,code,key,years)
        mtpc4m2.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False)                 
    df.to_csv('land_use.csv',index = False)
    
    
    missing = ec.check(df, code, relevant_years,key,missing)
            
    if missing == 1 :
        if diagram.get(key).get("minor3") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year3b,parameters.get("exeptions").get(code).get("end")+1))]
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year3b))]
            cpc2m3.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
        if diagram.get(key).get("minor2") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year2b,parameters.get("exeptions").get(code).get("end")+1))]      
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year2b))]
            cpc2m2.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
        if diagram.get(key).get("minor1") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year1b,parameters.get("exeptions").get(code).get("end")+1))]                 
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),year1b))]
            cpc2m1.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
    
    
    return df,dfs
 