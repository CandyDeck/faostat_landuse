import yaml

with open(r'diagram.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    diagram = yaml.load(file, Loader=yaml.FullLoader)
 
def check(df, code, relevant_years,key,missing):
    if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        for years in relevant_years:
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1
        
    if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        for years in relevant_years:
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1
        
    if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        for years in relevant_years:
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1

    return 0