# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression
from pathlib import Path

#import download_files
import pandas as pd
import yaml
import time


with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'items_primary.yaml') as file:
    items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'diagram.yaml') as file:
    diagram = yaml.load(file, Loader=yaml.FullLoader)

    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]


storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"


df = pd.read_csv('land_use_test.csv', encoding="latin-1") 

#df_iso3 = pd.read_csv('../../land_use/data/refreshed_production_crop.csv', encoding="latin-1") 
list_ISO3 = list(df['ISO3'])
country = [] 
for i in list_ISO3: 
    if i not in country: 
        country.append(i)
print(len(country))  
      
country=['AFG']
list_item_code = list(df['Item Code'])
FAOitem = []
for i in list_item_code:
    if i not in FAOitem:
        FAOitem.append(i)
#print(len(FAOitem))        
#print(items_primary)
#print(parameters.get("exeptions"))
col_years = [col for col in df.columns if  col.startswith("Y")]  
#FAOitem=[6630,6633,6640]
for code in country :
    #print(code)  
    for item in items_primary:
        if not code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                #print(item)
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1)))]

                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    print(code,item)

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            if first_year == parameters.get("year_of_interest").get("begin") :
                                print("sauter l etape",first_year,code)
                                #time.sleep(15)
                                break
                #print(relevant_years,backward)


                    #for years in backward: 
                    #    if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    #        last_year = int(years.replace("Y",""))
                    #        if last_year == parameters.get("year_of_interest").get("end") :
                    #            print("sauter l etape",last_year,code)
                    #            #time.sleep(15)
                    #            break  
                            
                            
                            else :
                                if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]].isnull().values.all():
                                    x = np.array([1, 2, 3]).reshape((-1, 1))
                                    begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                                    end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end"))]]).to_string(index=False,header=False))])
                                    print(begin, end)
                                    model = LinearRegression().fit(x, begin) 
                                    model2 = LinearRegression().fit(x, end)
                                    print('slope:', model.coef_,model2.coef_)
                                    if np.sign(model.coef_) == np.sign(model2.coef_):
                                        print("meme signe") 
                                        #break
                                        value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        print(value1)
                                        value1=float(value1.to_string(index=False, header=False))
                                        value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                        value2=float(value2.to_string(index=False, header=False))
                                        value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                        value3=float(value3.to_string(index=False, header=False))
                                        print("meme signe",value1,value2,value3)
                                        average=(value1+value2+value3)/3
                                        time.sleep(15)
                                    else :
                                        print("signes differents")
                                        for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                            print(years)
                                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                            value1=float(value1.to_string(index=False, header=False))
                                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                            value2=float(value2.to_string(index=False, header=False))
                                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                            value3=float(value3.to_string(index=False, header=False))
                                            print(value1,value2,value3)
                                            average=(value1+value2+value3)/3
                                            print(average)
                                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                    df.to_csv('land_use_regression_6630_6633_6640.csv',index = False)            
                           
                                break
            '''                
            if item in parameters.get("exeptions"):
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("end")+1))]
                print(item,relevant_years)
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    print(code,item)

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            if first_year == parameters.get("exeptions").get(item).get("begin") :
                                print("exeption,sauter l etape",first_year,code)
                                break
                            
                            else :
                                if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]].isnull().values.all():
                                    x = np.array([1, 2, 3]).reshape((-1, 1))
                                    begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                                    end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end"))]]).to_string(index=False,header=False))])
                                    print(begin, end)
                                    model = LinearRegression().fit(x, begin) 
                                    model2 = LinearRegression().fit(x, end)
                                    print('slope:', model.coef_,model2.coef_)
                                    if np.sign(model.coef_) == np.sign(model2.coef_):
                                        print("exeption meme signe") 
                                        #break
                                        value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        print(value1)
                                        value1=float(value1.to_string(index=False, header=False))
                                        value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                        value2=float(value2.to_string(index=False, header=False))
                                        value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                        value3=float(value3.to_string(index=False, header=False))
                                        print("meme signe",value1,value2,value3)
                                        average=(value1+value2+value3)/3
                                        time.sleep(15)
                                    else :
                                        print("exeption signes differents")
                                        for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                            print(years)
                                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                            value1=float(value1.to_string(index=False, header=False))
                                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                            value2=float(value2.to_string(index=False, header=False))
                                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                            value3=float(value3.to_string(index=False, header=False))
                                            print(value1,value2,value3)
                                            average=(value1+value2+value3)/3
                                            print(average)
                                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                    df.to_csv('land_use_regression_6630_6633_6640.csv',index = False)            
                           
                                break
                                '''
            #else :
            #    print(item,"item in exeption")    
            #    relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("end")+1))]
            #    print(relevant_years)    
          