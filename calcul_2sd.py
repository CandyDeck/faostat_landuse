import yaml

with open(r'small_diagrams.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    diagram = yaml.load(file, Loader=yaml.FullLoader)
    
def calculation(df,code,key,years):
    
    if not df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]].isnull().values.all():
        value_major = df.loc[(df['Item Code']==key)&(df['ISO3']==code),[years]]
        value_major=float(value_major.to_string(index=False, header=False))
        if not diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if not diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                    if df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]] = value_major
        if not diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if not diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if diagram.get(key).get("minor2") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                    if df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]] = value_major
                                  
        if not diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if not diagram.get(key).get("minor3") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                if diagram.get(key).get("minor1") in df.loc[df['ISO3']==code, ["Item Code"]].values  :
                    if df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                        df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]] = value_major
                            
    return df