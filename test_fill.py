from pathlib import Path

#import download_files
import pandas as pd
import yaml


with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'items_primary.yaml') as file:
    items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'diagram.yaml') as file:
    diagram = yaml.load(file, Loader=yaml.FullLoader)

    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)

df = pd.read_csv('land_use.csv', encoding="latin-1") 

relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
col_years = [col for col in df.columns if  col.startswith("Y")] 


storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"



list_ISO3 = list(df['ISO3'])
country = [] 
for i in list_ISO3: 
    if i not in country: 
        country.append(i)
 
      


for code in country :
    print(code)
    for item in items_primary:
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            for years in relevant_years:
                if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    first_year = int(years.replace("Y",""))
                    years_of_interest=list(range(first_year,parameters.get("year_of_interest").get("end")+1))
                    all_years =[make_valid_fao_year(year) for year in years_of_interest]
                    for years in all_years:
                        #print("all_years",years,all_years)
                        if  df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            #print('empty cell',years,item)
                            first_empty_year = int(years.replace("Y",""))
                            prev_year = first_empty_year-1
                            non_empty_value=df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(prev_year)]]
                            non_empty_value = non_empty_value.to_string(index=False, header=False)
                            non_empty_value=float(non_empty_value)
                            #print(years,code, prev_year,non_empty_value)
                        
                            years_of_interest=[make_valid_fao_year(years) for years in list(range(first_empty_year+1,parameters.get("year_of_interest").get("end")+1))]
                            #print("year_of_interest",years_of_interest)
                            #continue
                            for years in years_of_interest:
                                #print(code, years)
                                #print(years, df.loc[(df['ISO3']==code)&(df['Item Code']==item),[years]])
                                #if df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                                    #print("elles sont toutes vides apres",years)
                                if not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                                    #print("non empty",years)
                                    next_non_empty = int(years.replace("Y",""))
                                    non_empty_value2=df.loc[(df['ISO3']==code)&(df['Item Code']==item),[years]]
                                    non_empty_value2 = non_empty_value2.to_string(index=False, header=False)
                                    non_empty_value2=float(non_empty_value2)
                                    nber_empty_cells = next_non_empty - prev_year -1
                                    #rint("next non empty",next_non_empty)
                                    #print('1st',non_empty_value,'2d',non_empty_value2)
                                    increment=(non_empty_value2-non_empty_value) / (nber_empty_cells+1)                               
                                    #print('increament',increment)
                                    n=1
                                    for years in [make_valid_fao_year(year) for year in list(range(first_empty_year,next_non_empty))] :
                                        #print("years",years)
                                        df.loc[(df['ISO3']==code)&(df['Item Code']==item),[years]]=non_empty_value +n*increment
                                        #print(years,non_empty_value +n*increment )
                                        n=n+1
                                    df.to_csv('land_use_fill.csv',index = False)
                                    break 
                                
                            continue
                        continue
                       
            df.to_csv('land_use_fill.csv',index = False)
