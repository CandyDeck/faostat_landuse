import calcul_1 as cal1
import calcul_2 as cal2
import make_table_pourcentage as mtp
import empty_cells as ec
import calcul_pourcent_all as cpa
import adjustment as adj

def solve(df, dfs,code,relevant_years, diagram,key,country,missing):
    for years in relevant_years :
        if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            cal2.calculation(df,code,key,years)       
            cal1.calculation(df,code,key,years) 
    
    
    mtp.make_table(df, dfs,code,relevant_years, diagram,key,country) 
    file_name = str(key)+".csv"  #Change the column name accordingly
    dfs[key].to_csv(file_name, index=False)
    df.to_csv('land_use.csv',index = False)

    missing = ec.check(df, code, relevant_years,key,missing)
    if missing == 1:
        for years in relevant_years : 
            cpa.pourcentage(df, dfs,code,years, diagram,key,country)   
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)

    
    return df,dfs
