# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression
import yaml

def regression(code,parameters,df):
    
    with open(r'unique_items.yaml') as file:
        unique_items = yaml.load(file, Loader=yaml.FullLoader)    
    
    def make_valid_fao_year(year):
        """Make a valid fao year string from int year"""
        return "Y" + str(year)
    
    #print(unique_items)
    for item in unique_items:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
        backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1)))]
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            for years in relevant_years:
                if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    first_year = int(years.replace("Y",""))
                    break
            for years in backward:
                if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    last_year = int(years.replace("Y",""))
                    break
                    
            for years in backward:
                if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                    value=float(value.to_string(index=False, header=False))
                    year_zero = int(years.replace("Y",""))
                    if value == 0 :
                        for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                            print(code, item)           
  
            if first_year==last_year :

                value_unique = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                value_unique=float(value_unique.to_string(index=False, header=False))
                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=value_unique
                df.to_csv('itemland_use_regression2.csv',index = False)    
                continue
            if (last_year - first_year==1) :
                value_1 = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                value_1=float(value_1.to_string(index=False, header=False))
                value_2 = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                value_2=float(value_2.to_string(index=False, header=False))
                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=(value_1+value_2)/2
                df.to_csv('itemland_use_regression2.csv',index = False) 
                continue
            
            else :
                if  not last_year == parameters.get("year_of_interest").get("end"):
                    if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]].isnull().values.all():
                        x = np.array([1, 2, 3]).reshape((-1, 1))
                        begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                        end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                                        
                        model = LinearRegression().fit(x, begin) 
                        model2 = LinearRegression().fit(x, end)
                        #print(code, item)
                        if  np.sign(model.coef_) == np.sign(model2.coef_):

                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                            value1=float(value1.to_string(index=False, header=False))
                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                            value2=float(value2.to_string(index=False, header=False))
                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                            value3=float(value3.to_string(index=False, header=False))
                                
                            value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                            value4=float(value4.to_string(index=False, header=False))
                            value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                            value5=float(value5.to_string(index=False, header=False))
                            value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                            value6=float(value6.to_string(index=False, header=False))
                                
                            average=(value1+value2+value3)/3
                            average2=(value4+value5+value6)/3
                            x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                            linear = np.array([average, average2])
                            model3=LinearRegression().fit(x, linear)
                            for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                if model3.coef_ * years +model3.intercept_ >= 0 :
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                else :
                                    previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    previous_value = float(previous_value.to_string(index=False, header=False))
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value
                            df.to_csv('itemland_use_regression2.csv',index = False)         
                                
        
                        else :
                            for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                value3=float(value3.to_string(index=False, header=False))
                                average=(value1+value2+value3)/3
                                df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                            df.to_csv('itemland_use_regression2.csv',index = False)            
  
                                                                               
            
    return df