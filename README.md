# Dealing with missing values #
We would like to use [FAOSTAT](http://www.fao.org/faostat/en/#data) as a unique source of data. This unique source has missing entries. We need to make assumption in order to get a complete table before to go further.
Here we will explain as a step by step process, the different assumption we made.
The diagram below summarize the different relations used in this code in order to fill empty cells.
![diagram_final.png](diagram_final.png)
From the diagram, we will fill the empty cells in a 2 step process.
- A simple calculation with a simple operation is implemented in the code.

For exemple, Item 6600 = Item 6601 + Item 6680 + Item 6773(from 2007)
- For each "main item", we look at the distribution of the value unto the "minor item".
For exemple, we tale the value of item 6600 for a particular year and we look at the pourcentage attributed to item 6601, item 6680 and item 6773. If the standard deviation of the pourcentage for one minor item (for all the year available) is below a certain value, we take the mean value of the pourcentage for this particulat item and we do apply this mean pourcentage in order to calculate the missing value.

The module (**download_files.py**) import the raw data ([Input land use](http://www.fao.org/faostat/en/#data/RL)) and store it at the location **land_use/data**

The main script landuse.py reads the data and tend to fill empty cells.
Looking at the diagram above, we can notice a "pyramidal" of the land use.

The main script reads 2 yaml files : 

- diagram.yaml

- parameters.yaml

**diagram.yaml** contains the relations between a "major" item and its "minor"
For exemple, item 6600 (Country area) corresponds to the sum of item 6601 (Land area), item 6680 (Inland waters) and item 6773 (Coastal waters)

**parameters.yaml** contains the first and the last year of interest. It also contains information on some exeption where these years of interest are not applicable.
This is the case for some countries created after 1995 or for some FAO items which are taken into consideration at a later date.
For exemple, South Soudan (ISO3 : SSD) is created in 2011.
FAO item 6773 (Coastal waters) is registered from 2007.

The main diagram calls 4 modules : case1.py, case2.py, case3.py and case4.py
Each of these modules follows the main 2 steps : 
- Try to fill empty cells by following the simple mathematical equation : 
**major = minor1 + minor2 + minor3**
- Look at the distribution of the major item over the minor ones.
For the known values, calculate the mean and the standard deviation.
If the standard deviation for the available data is less than 5, obtain the missing minor value by doing :
**minor = mean * major /100**
After the second method, we check if the the mathematical equation :
**major = minor1 + minor2 + minor3**
is still true. If it is not the case, we adjust the values of the minor items in order to get the right result over the sum.

case1.py, case2.py case3.py and case4.py just depend on the different cases we could face :
- **case1** : the country of interest and the minor items are not in the exeptions list contained in **parameters.yaml**
- **case2** : the country of interest is not in the exeptions list but one of the minor items is
- **case3** : the country of interest is in the exeptions list but none of the minor item is
- **case4** : Both, the country of interest and one of the minor item are in the exeptions list


When the 2 main steps are performed, we carry on by doing interpolation in order to fill empty cells surrounded by non empty cells.

Finally, we do some extrapolation. The graph below represents a typical case where there are a number of missing points at the beginning of the sample. On this graph, we can see that point 1 to point 5 included are missing.

![known_points.png](known_points.png)

We decided to follow 2 differents directions in order to fill the empty cells.
First, we choose to select the 3 first known values (red points on the graph below) and the last three (yellow points).
![method1.png](method1.png)
We proceed to a linear regression on these 2 lots of points.
We then need to compare the slopes of the 2 linear regressions.
On the exemple below, we can see the slopes are of diferent sign.
In this case, the missing values are calculated as followed :
we calculate the average of the 3 first known values (red points).
This average is allocated to the point just before the first known value. We carry on descending from the first unknown year to the first year of interest :

```python
if  not np.sign(model.coef_) == np.sign(model2.coef_):
    for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
        value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
        value1=float(value1.to_string(index=False, header=False))
        value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
        value2=float(value2.to_string(index=False, header=False))
        value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
        value3=float(value3.to_string(index=False, header=False))
        average=(value1+value2+value3)/3
        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
```
Through this method, we obtain the missing points represented in red on the graph below.
![method1_final.png](method1_final.png)

We can also face a case where the 2 slopes are of the same sign : 
![method2.png](method2.png)
```python
if  np.sign(model.coef_) == np.sign(model2.coef_):

    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
    value1=float(value1.to_string(index=False, header=False))
    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
    value2=float(value2.to_string(index=False, header=False))
    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
    value3=float(value3.to_string(index=False, header=False))
                                
    value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]]
    value4=float(value4.to_string(index=False, header=False))
    value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]
    value5=float(value5.to_string(index=False, header=False))
    value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]
    value6=float(value6.to_string(index=False, header=False))
                                
    average=(value1+value2+value3)/3
    average2=(value4+value5+value6)/3
    x = np.array([first_year+1, parameters.get("year_of_interest").get("end")-1]).reshape((-1, 1))
    linear = np.array([average, average2])
    model3=LinearRegression().fit(x, linear)
    for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
```    
In this case, we calculate the average of the first 3 points and the average of the last 3 points.
Knowing these 2 values, we calculate the linear regression passing these 2 points (green points and green line on the graph below). In order to determine the missing values, we simply apply the latest equation (linear regression). 
![method2_2.png](method2_2.png)

_**Remarks regarding the extrapolation's step :**
1. If the last value is 0 (based on actual data) we continue with zero.
2. If we reach zero through an extrapolation, we take the last non zero value and fill the rest of the empty cells with it._


The main script **landuse.py** is written as follow :
1. We deal first with the main diagram on the left hand side of the first picture. This means we deal with all the values going from FAO item 6600 (Country area) to items 6630 (temporary crops), item 6633 (temporary meadows) and item 6640 (Temporary fallow).
```python
#CASE1
if not a and not code in parameters.get("exeptions"):
     case1.solve(df, dfs,code,relevant_years, diagram,key,country,missing)
#CASE2
if a and code not in parameters.get("exeptions"):
    case2.solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a)
#CASE3
if not a and code in parameters.get("exeptions"):
    case3.solve(df, dfs,code,relevant_years, diagram,key,country,missing)      
#CASE4
if a and code in parameters.get("exeptions"):
    case4.solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a)
```

2. We interpolate in order to fill empty cells surrounded by non empty cells for the items composing this main diagram (the list of items is in the **items_primary.yaml** file)
```python
for code in country :
    for item in items_primary:
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            fill_cells.fill(code, item, relevant_years,df,parameters)
```

3. We make sure the mathematical relation **major = minor1 + minor2 + minor3** is valid :         
```python
for code in country :
    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        for years in relevant_years :
            adj2.adjust(df,code,years, diagram,key,country)
```
If not, we adjust : 
```python
value_minor1= value_minor1*value_major/(value_minor1+value_minor2+value_minor3)
value_minor2= value_minor2*value_major/(value_minor1+value_minor2+value_minor3)value_minor3= value_minor3*value_major/(value_minor1+value_minor2+value_minor3)
                
df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor1")),[years]]=value_minor1
df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor2")),[years]]=value_minor2
df.loc[(df['ISO3']==code)&(df['Item Code']==diagram.get(key).get("minor3")),[years]]=value_minor3
```
4. We use the regression method in order to fill the empty cells at the beginning or at the end of the data sample.
```python
for code in country :
    reg.regression(code,parameters,df)
```
Folowwing this step, we check again if the mathematical relation **major = minor1 + minor2 + minor3** is still valid and adapt the values of the minor items in case this is not true.

5. After we dealt with the main diagram, we deal with the "lonely" items, the one that do not directly depend on another.
The list of these are contained in the yaml file called **unique_items.yaml**.
For these items, only the interpolation and the regression procedure are applied.

6. Finally, we deal with the small diagrams on the right hand side of the first picture. Here, we use the items listed in the files : **small_diagrams.yaml** and **items_small_diagrams.yaml**
Steps 1 to 4 are applied.




