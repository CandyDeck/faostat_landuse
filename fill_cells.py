   
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


def fill(code, item, relevant_years,df,parameters):
    for years in relevant_years:
        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
            first_year = int(years.replace("Y",""))
            years_of_interest=list(range(first_year,parameters.get("year_of_interest").get("end")+1))
            all_years =[make_valid_fao_year(year) for year in years_of_interest]
            for years in all_years:
                if  df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                    first_empty_year = int(years.replace("Y",""))
                    prev_year = first_empty_year-1
                    non_empty_value=df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(prev_year)]]
                    non_empty_value = non_empty_value.to_string(index=False, header=False)
                    non_empty_value=float(non_empty_value)
                    years_of_interest=[make_valid_fao_year(years) for years in list(range(first_empty_year+1,parameters.get("year_of_interest").get("end")+1))]
                
                    for years in years_of_interest:
                        if not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            next_non_empty = int(years.replace("Y",""))
                            non_empty_value2=df.loc[(df['ISO3']==code)&(df['Item Code']==item),[years]]
                            non_empty_value2 = non_empty_value2.to_string(index=False, header=False)
                            non_empty_value2=float(non_empty_value2)
                            nber_empty_cells = next_non_empty - prev_year -1
                            increment=(non_empty_value2-non_empty_value) / (nber_empty_cells+1) 
                            n=1
                            for years in [make_valid_fao_year(year) for year in list(range(first_empty_year,next_non_empty))] :
                                df.loc[(df['ISO3']==code)&(df['Item Code']==item),[years]]=non_empty_value +n*increment
                                n=n+1
                            df.to_csv('land_use_fill.csv',index = False)
                            break 
                                
                    continue
                continue
                       
    df.to_csv('land_use_fill.csv',index = False)
    
    return df

