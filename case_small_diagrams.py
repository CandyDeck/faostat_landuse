import calcul_1sd as cal1sd
import calcul_2sd as cal2sd
import make_table_pourcentage_small as mtps
import empty_cells_small_diagrams as ecsd
import calcul_pourcent_all as cpa
import adjustment as adj

def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)

def solve(df, dfs,code,relevant_years, small_diagrams,key,country,missing,parameters):
    for years in relevant_years :
        if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            if  not df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]].isnull().values.all():
                first_year = int(years.replace("Y",""))
                relevant_years_2 = [make_valid_fao_year(year) for year in list(range(first_year,parameters.get("year_of_interest").get("end")+1))]
        
                for years in relevant_years_2:
                    cal2sd.calculation(df,code,key,years)       
                    cal1sd.calculation(df,code,key,years)
            
        
                mtps.make_table(df, dfs,code,relevant_years_2, small_diagrams,key,country) 
                file_name = str(key)+".csv"  #Change the column name accordingly
                dfs[key].to_csv(file_name, index=False)
                df.to_csv('land_use3.csv',index = False)
   
                missing = ecsd.check(df, code, relevant_years_2,key,missing)
                if missing == 1:
                    for years in relevant_years_2 : 
                        cpa.pourcentage(df, dfs,code,years, small_diagrams,key,country)   
                        adj.adjust(df, dfs,code,years, small_diagrams,key,country)
                    df.to_csv('land_use3.csv',index = False)
        
    
    return df,dfs

