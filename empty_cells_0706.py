import yaml

with open(r'diagram.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    diagram = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'parameters.yaml') as file:
    # The FullLoader parameter handles the conversion from YAML
    # scalar values to Python the dictionary format
    parameters = yaml.load(file, Loader=yaml.FullLoader)

def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year) 

def check(df, code, years,key,missing):
    if diagram.get(key).get("minor1") in parameters.get("exeptions"):
        exeption1=diagram.get(key).get("minor1")

        year1b=parameters.get("exeptions").get(exeption1).get("begin")
        year1e=parameters.get("exeptions").get(exeption1).get("end")
    else :
        year1b=parameters.get("year_of_interest").get("begin")
        year1e=parameters.get("year_of_interest").get("end")
        
        
    if diagram.get(key).get("minor2") in parameters.get("exeptions"):
        exeption2=diagram.get(key).get("minor2")

        year2b=parameters.get("exeptions").get(exeption2).get("begin")
        year2e=parameters.get("exeptions").get(exeption2).get("end")
    else :
        year2b=parameters.get("year_of_interest").get("begin")
        year2e=parameters.get("year_of_interest").get("end")
        
    if diagram.get(key).get("minor3") in parameters.get("exeptions"):
        exeption3=diagram.get(key).get("minor3")

        year3b=parameters.get("exeptions").get(exeption3).get("begin")
        year3e=parameters.get("exeptions").get(exeption3).get("end")
    else :
        year3b=parameters.get("year_of_interest").get("begin")
        year3e=parameters.get("year_of_interest").get("end")

    if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        relevant_years_1 = [make_valid_fao_year(year) for year in range(year1b,year1e+1)]
        for years in relevant_years_1:
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1
            
                
    if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        relevant_years_2 = [make_valid_fao_year(year) for year in range(year2b,year2e+1)]
        for years in relevant_years_2 :
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1
                      
    if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
        relevant_years_3 = [make_valid_fao_year(year) for year in range(year3b,year3e+1)]
        for years in relevant_years_3 :
            if  df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all():
                return 1
            
    return 0
