from pathlib import Path

#import download_files
import numpy as np
import pandas as pd
import yaml
import fill_country_area as fca
import case1 
import case2
import case3
import case4
import case_small_diagrams as csd
import fill_cells
import adjustment2 as adj2
import regression_implant as reg
import regression_implant2 as reg2
import regression_implant3 as reg3

with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'items_primary.yaml') as file:
    items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'diagram.yaml') as file:
    diagram = yaml.load(file, Loader=yaml.FullLoader)


with open(r'unique_items.yaml') as file:
    unique_items = yaml.load(file, Loader=yaml.FullLoader)    
    
with open(r'small_diagrams.yaml') as file:
    small_diagrams = yaml.load(file, Loader=yaml.FullLoader) 

with open(r'items_small_diagrams.yaml') as file:
    items_small_diagrams = yaml.load(file, Loader=yaml.FullLoader) 
    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]


storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"


#df_iso3 = pd.read_csv('land_use.csv', encoding="latin-1") 

df_iso3 = pd.read_csv('../../land_use/data/refreshed_production_crop.csv', encoding="latin-1") 
list_ISO3 = list(df_iso3['ISO3'])
country = [] 
for i in list_ISO3: 
    if i not in country: 
        country.append(i)
#print(len(country))  
      
df = pd.read_csv(data_path/'refreshed_land_use.csv', encoding="latin-1") 
#df = pd.read_csv('land_use.csv', encoding="latin-1")
list_item_code = list(df['Item Code'])
FAOitem = []
for i in list_item_code:
    if i not in FAOitem:
        FAOitem.append(i)
print(len(FAOitem))        

col_years = [col for col in df.columns if  col.startswith("Y")] 
  
meta_col = ["ISO3", "Item Code", "Item","Unit"] 
        
df=df[meta_col + relevant_years]
df = df[df['Unit'] != 'million tonnes']
df = df[df['ISO3'] != 'not found']

df=df[df.ISO3.isin(list_ISO3)]
df.isnull().sum().sum()
for code in country :
    fca.fill(df, code,col_years)

#CREATION DES DATAFRAMES

dfs = dict()

for key in diagram:
    df1 = pd.DataFrame(columns = ['ISO3','item'])
    for year in relevant_years:
        df1[year]=""
    for code in country :
       for item in list(diagram.get(key).values()) : 
           df1 = df1.append(pd.Series([code,item,0], index=['ISO3','item',year]),ignore_index=True)
    df1=df1.fillna(0)
    dfs[key] = df1.copy()

for key in small_diagrams:
    df1 = pd.DataFrame(columns = ['ISO3','item'])
    for year in relevant_years:
        df1[year]=""
    for code in country :
       for item in list(small_diagrams.get(key).values()) : 
           df1 = df1.append(pd.Series([code,item,0], index=['ISO3','item',year]),ignore_index=True)
    df1=df1.fillna(0)
    dfs[key] = df1.copy()


for code in country :
    #50 minutes
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        missing=0
        year1b=year2b=year3b=parameters.get("year_of_interest").get("begin")
        year1e=year2e=year3e=parameters.get("year_of_interest").get("end")
        a=[]
        if diagram.get(key).get("minor1") in parameters.get("exeptions"):
            exeption1=diagram.get(key).get("minor1")
            year1b=parameters.get("exeptions").get(exeption1).get("begin")
            year1e=parameters.get("exeptions").get(exeption1).get("end")
            a.append(year1b)
        if diagram.get(key).get("minor2") in parameters.get("exeptions"):
            exeption2=diagram.get(key).get("minor2")
            year2b=parameters.get("exeptions").get(exeption2).get("begin")
            year2e=parameters.get("exeptions").get(exeption2).get("end")
            a.append(year2b)
        if diagram.get(key).get("minor3") in parameters.get("exeptions"):
            exeption3=diagram.get(key).get("minor3")
            year3b=parameters.get("exeptions").get(exeption3).get("begin")
            year3e=parameters.get("exeptions").get(exeption3).get("end")
            a.append(year3b)
            
        #CASE1
        if not a and not code in parameters.get("exeptions"):
            case1.solve(df, dfs,code,relevant_years, diagram,key,country,missing)

        #CASE2
        if a and code not in parameters.get("exeptions"):
            case2.solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a)
        
        #CASE3
        if not a and code in parameters.get("exeptions"):
            case3.solve(df, dfs,code,relevant_years, diagram,key,country,missing)
        
        #CASE4
        
        if a and code in parameters.get("exeptions"):
            case4.solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a)


#18min#
print("fill")
for code in country :
    print(code)
    for item in items_primary:
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            fill_cells.fill(code, item, relevant_years,df,parameters)
            

#11min#          
print("ADJUST")
for code in country :
    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        for years in relevant_years :
            adj2.adjust(df,code,years, diagram,key,country)
        df.to_csv('test.csv',index = False)
#1min#       
print("REGRESSION")   
for code in country :
    reg.regression(code,parameters,df)

#8min#
print("ADJUST2")
for code in country :
    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in diagram:
        for years in relevant_years :
            adj2.adjust(df,code,years, diagram,key,country)
        df.to_csv('land_use_adjust2.csv',index = False)

#7min#
print("fill")
for code in country :
    print(code)
    for item in unique_items:
        print(item)
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            fill_cells.fill(code, item, relevant_years,df,parameters)

 
#1min#     
for code in country :
    reg2.regression(code,parameters,df)

#1h25#
for code in country :
    print(code)

    for key in small_diagrams:
        missing=0
        print(key)            
        csd.solve(df, dfs,code,relevant_years, small_diagrams,key,country,missing,parameters)


#10min#
for code in country :
    print(code)
    for item in items_small_diagrams:
        print(item)
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            fill_cells.fill(code, item, relevant_years,df,parameters)
#1min#
for code in country :
    print(code)
    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

    for key in small_diagrams:
        print(key)
        for years in relevant_years :
            adj2.adjust(df,code,years, small_diagrams,key,country)
        df.to_csv('land_use_adjust3.csv',index = False)
#1min#      
for code in country :
    reg3.regression(code,parameters,df)

df.to_csv('final.csv',index = False)