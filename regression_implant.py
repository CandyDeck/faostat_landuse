# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression
import yaml

def regression(code,parameters,df):

    with open(r'items_primary.yaml') as file:
        items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
    
    def make_valid_fao_year(year):
        """Make a valid fao year string from int year"""
        return "Y" + str(year)

    for item in items_primary:
        if not code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1)))]

                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            break

                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                    print(code, item)
                    if first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end"):
                        continue
                                                                            
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("year_of_interest").get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([parameters.get("year_of_interest").get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
  
                                                                            
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                             
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                
                                
                                
                                
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 

                                             
            if  item in parameters.get("exeptions"):
                relevant_years=[make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(item).get("end")+1))]
                backward=[make_valid_fao_year(year) for year in list(reversed(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(item).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            break

                           
                    if first_year == parameters.get("exeptions").get(item).get("begin") and last_year == parameters.get("exeptions").get(item).get("end"):
                        continue
                                                                            
                    
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                    print(code, item)
                    
                    
                    if not first_year == parameters.get("exeptions").get(item).get("begin") and last_year == parameters.get("exeptions").get(item).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
  
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("exeptions").get(item).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("exeptions").get(item).get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                    
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(item).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("exeptions").get(item).get("begin") and not last_year == parameters.get("exeptions").get(item).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([parameters.get("exeptions").get(item).get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                     
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
        
                    if not first_year == max(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("begin")) and not last_year == parameters.get("exeptions").get(item).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        print ("value avant 0",code, item)
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                      
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                for years in range(first_year-1,max(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("begin"))-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                 
        if  code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            break
       
                    if first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end"):
                        continue
                                                                            
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                    print(code, item)
                                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("exeptions").get(code).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                   
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average

                                
                                df.to_csv('itemland_use_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([parameters.get("exeptions").get(code).get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                        
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
 
                                                                            
                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value              
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                   
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 


                        
            if  item in parameters.get("exeptions"):
                relevant_years=[make_valid_fao_year(year) for year in list(range(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")),parameters.get("exeptions").get(code).get("end")+1))]
                backward=[make_valid_fao_year(year) for year in list(reversed(range(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")),parameters.get("exeptions").get(code).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :

                    for years in relevant_years:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            break

                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                    print(code, item)
                                    
                    if first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and last_year == parameters.get("exeptions").get(code).get("end"):
                        continue
                                                                            
                    
                    if not first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and last_year == parameters.get("exeptions").get(code).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("exeptions").get(code).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                     
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            

                    
                    if  first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and not last_year == parameters.get("exeptions").get(item).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                     
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
  
                    if not first_year == max(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(code).get("begin")) and not last_year == parameters.get("exeptions").get(code).get("end"):
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                            
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                             
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                for years in range(first_year-1,max(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(code).get("begin"))-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
