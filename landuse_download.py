
import zipfile
import pandas as pd
import requests
import country_converter as coco
from pathlib import Path


def download_fao_data(src_url, storage_path, force_download=False):
    """Store the fao dataset at storage path

    Parameters
    ----------

    src_url: str
        Url of the source data

    storage_path: pathlib.Path
        Location for storing the data

    force_download: boolean, optional
        If True, downloads the data even if it is already present in storage_path.
        If False (default), only downloads the data if is is not available locally.

    Returns
    -------
        Downloaded File: pathlib.Path

    KST-CD: What I did here are numpy-style docstrings. Please use these in this format for documenting the use of functions. There are more fields defined in the numpy docstrings, but at the very minimum use parameters.
    """
    filename = Path(src_url.split("/")[-1])

    # Making the storage path if should not exisit
    storage_path.mkdir(parents=True, exist_ok=True)
    storage_file = storage_path / filename
    if storage_file.exists() and (force_download is False):
        return storage_file
    download = requests.get(src_url)

    # Raise exception if the file is not available
    download.raise_for_status()
    with open(storage_file, "wb") as sf:
        sf.write(download.content)

    return storage_file


def extract_archive(zip_archive, store_to):
    """Extract zip archive (pathlib.Path) to store_to (pathlib.Path)
    KST-CD: If it is really(!) simple you can get away with very short docstrings - rules are fluid
    """
    with zipfile.ZipFile(zip_archive, "r") as zf:
        zf.extractall(path=store_to)



def read_land_data(data_file, relevant_years=None):
    """Reads the data and returns dataframe

    Parameters
    ----------

    data_file: pathlib.Path
        Extracted fao csv file

    relevant_years: list(str)
        Relevant years defined in the main : relevant_years = list(range(1995,2021))
        Select the years of interest, in this case from 1995 to 2020

    """
    

    df = pd.read_csv(data_file, encoding="latin-1")

    if relevant_years:

        country_code = list(df['Area Code'])
        converter=coco.country_converter
        
        df['ISO3']=converter.convert(names = country_code, src = 'FAOcode', to='ISO3')
                    
        meta_col = [col for col in df.columns if not col.startswith(("Y", "key", "Element","Area"))] 
        
        return df[meta_col + relevant_years]
    
    
    else:
        return df

    
  
        
        
def deal_missing_data(data_file, relevant_years=None):
    """Reads the data and returns dataframe

    Parameters
    ----------

    data_file: pathlib.Path
        Extracted fao csv file

    relevant_years: list(str)
        Relevant years defined in the main : relevant_years = list(range(1995,2021))
        Select the years of interest, in this case from 1995 to 2020

    """
    

    df = pd.read_csv(data_file, encoding="latin-1")

    if relevant_years:

        country_code = list(df['Area Code'])
        converter=coco.country_converter
        
        df['ISO3']=converter.convert(names = country_code, src = 'FAOcode', to='ISO3')
        filter_col=[col for col in df if col.startswith('Y')]
        df_modified = df.copy()
        
        df_modified['max_value'] = df_modified[filter_col].max(axis=1)
        df_modified['min_value'] = df_modified[filter_col].min(axis=1)
        df_modified['mean_value'] = df_modified[filter_col].mean(axis = 1, skipna = True)
        df_modified['std_dev'] = df_modified[filter_col].std(axis = 1, skipna = True)
        df_modified['Var']=df_modified[filter_col].var(axis = 1, skipna = True)
        
        #df['min_value'] = df.min(axis=1)
        #df['mean_value'] = df.mean(axis = 1, skipna = True)
        #df['std_dev'] = df.std(axis = 1, skipna = True)      
        analysis_col = [col for col in df_modified.columns if col.startswith(("max_value", "min_value", "mean_value","std_dev","Var"))]
        meta_col = [col for col in df_modified.columns if not col.startswith(("Y", "key", "Element","Area","max_value", "min_value", "mean_value","std_dev","Var"))] 

        #return df[meta_col + relevant_years]
        #print(df_modified.head(150))
        df_modified=df_modified[meta_col + relevant_years + analysis_col]
        print(df_modified.head(150))
       
        
        for index, row in df_modified.iterrows():
            for year in relevant_years:
                if row['std_dev']==0.0 and row['mean_value']!=0 and pd.isnull(row[year]):
                   print (year,row[year],row['std_dev'],row['mean_value'])
                   df_modified.loc[index,year] = row['mean_value']
                 
                
        return df_modified[meta_col + relevant_years + analysis_col]
    
    else:
        return df_modified
    
  
        
        
        
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


def get_missing_data(df):
    """Make a summary table with all missing data

    Any row with a nan is included in the resulting table

    fao_df: pandas DataFrame
        Based on raw csv read

    """
    return df[df.isnull().any(1)]


