# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression
from pathlib import Path

#import download_files
import pandas as pd
import yaml

with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'items_primary.yaml') as file:
    items_primary = yaml.load(file, Loader=yaml.FullLoader)
    
with open(r'diagram.yaml') as file:
    diagram = yaml.load(file, Loader=yaml.FullLoader)

    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]


storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"


df = pd.read_csv('land_use_fill.csv', encoding="latin-1") 

#df_iso3 = pd.read_csv('../../land_use/data/refreshed_production_crop.csv', encoding="latin-1") 
list_ISO3 = list(df['ISO3'])
country = [] 
for i in list_ISO3: 
    if i not in country: 
        country.append(i)
      
#country=['AFG']
list_item_code = list(df['Item Code'])
FAOitem = []
for i in list_item_code:
    if i not in FAOitem:
        FAOitem.append(i)
#print(len(FAOitem))        
#print(items_primary)
#print(parameters.get("exeptions"))
col_years = [col for col in df.columns if  col.startswith("Y")]  
#FAOitem=[6630,6633,6640]
#items_primary=[6616]
#country=['AFG']
for code in country :
    print(code)  
    
    for item in items_primary:
        #print(parameters.get("exeptions").get(6773).get("begin"))
        if not code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                #print(item)
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1)))]

                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    #print(code,item)

                    for years in relevant_years:
                        #print("first year", years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            #print("first year",first_year)
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            #print("last years",last_year)
                            break
                    #if  last_year == parameters.get("year_of_interest").get("end") :
                    #    print("FINAL last_year",last_year)
                    #    continue
                    #print(code,item,first_year,last_year)
                           
                    if first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end"):
                        #print("exeption,sauter l etape",first_year,code)
                        continue
                                                                            
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end"))]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, parameters.get("year_of_interest").get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break
                    
                    if  first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            #print("REGARDER ICI CE QUIL SE PASSE",begin, end)
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            #time.sleep(15)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([parameters.get("year_of_interest").get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                #print("signes differents")
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break  
                                                                            
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break

                        
                        '''IF ITEM IN EXEPTION BUT NOT CODE'''                                                 
            if  item in parameters.get("exeptions"):
                relevant_years=[make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(item).get("end")+1))]
                backward=[make_valid_fao_year(year) for year in list(reversed(range(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(item).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    #print(code,item)

                    for years in relevant_years:
                        #print("first year", years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            #print("first year",first_year)
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            #print("last years",last_year)
                            break
                    #if  last_year == parameters.get("year_of_interest").get("end") :
                    #    print("FINAL last_year",last_year)
                    #    continue
                    #print(code,item,first_year,last_year)
                           
                    if first_year == parameters.get("exeptions").get(item).get("begin") and last_year == parameters.get("exeptions").get(item).get("end"):
                        #print("exeption,sauter l etape",first_year,code)
                        continue
                                                                            
                    
                    if not first_year == parameters.get("exeptions").get(item).get("begin") and last_year == parameters.get("exeptions").get(item).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("end"))]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, parameters.get("exeptions").get(item).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("exeptions").get(item).get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,parameters.get("exeptions").get(item).get("begin")-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break
                    
                    if  first_year == parameters.get("exeptions").get(item).get("begin") and not last_year == parameters.get("exeptions").get(item).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(item).get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            #print("REGARDER ICI CE QUIL SE PASSE",begin, end)
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            #time.sleep(15)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([parameters.get("exeptions").get(item).get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                #print("signes differents")
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break  
                    if not first_year == max(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("begin")) and not last_year == parameters.get("exeptions").get(item).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,max(parameters.get("exeptions").get(item).get("begin"),parameters.get("year_of_interest").get("begin"))-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break

########################ICI C EST CODE DANS EXEPTION###########################################

        if  code in parameters.get("exeptions"):
            #print(code)
            if not item in parameters.get("exeptions"):
                #print(item)
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1)))]
                #print(relevant_years,backward)
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    #print(code,item)

                    for years in relevant_years:
                        #print("first year", years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            #print("first year",first_year)
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            #print("last years",last_year)
                            break
                    #if  last_year == parameters.get("year_of_interest").get("end") :
                    #    print("FINAL last_year",last_year)
                    #    continue
                    #print(code,item,first_year,last_year)
                           
                    if first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end"):
                        continue
                                                                            
                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, parameters.get("exeptions").get(code).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average

                                
                                df.to_csv('itemland_use_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            #print("REGARDER ICI CE QUIL SE PASSE",begin, end)
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            #time.sleep(15)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([parameters.get("exeptions").get(code).get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                #print("signes differents")
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break  
                                                                            
                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end"):
                        #print("not and not")
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break

                        
                        '''IF ITEM and  CODE in exeption'''                                                 
            if  item in parameters.get("exeptions"):
                relevant_years=[make_valid_fao_year(year) for year in list(range(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")),parameters.get("exeptions").get(code).get("end")+1))]
                backward=[make_valid_fao_year(year) for year in list(reversed(range(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")),parameters.get("exeptions").get(code).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    #print(code,item)

                    for years in relevant_years:
                        #print("first year", years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            #print("first year",first_year)
                            break
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            #print("last years",last_year)
                            break
                    #if  last_year == parameters.get("year_of_interest").get("end") :
                    #    print("FINAL last_year",last_year)
                    #    continue
                    #print(code,item,first_year,last_year)
                           
                    if first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and last_year == parameters.get("exeptions").get(code).get("end"):
                        #print("exeption,sauter l etape",first_year,code)
                        continue
                                                                            
                    
                    if not first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and last_year == parameters.get("exeptions").get(code).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(item).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, parameters.get("exeptions").get(code).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break
                    
                    if  first_year == max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")) and not last_year == parameters.get("exeptions").get(item).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            #print("REGARDER ICI CE QUIL SE PASSE",begin, end)
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            #time.sleep(15)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin")))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False)         
                                
        
                            else :
                                #print("signes differents")
                                for years in range(last_year+1,parameters.get("exeptions").get(item).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)            
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break  
                    if not first_year == max(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(code).get("begin")) and not last_year == parameters.get("exeptions").get(code).get("end"):
                        #print(first_year,last_year,item, code)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            #print(begin, end)
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print('slope:', model.coef_,model2.coef_)
                            if  np.sign(model.coef_) == np.sign(model2.coef_):
                                #print("meme signe") 
                                #break
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                #print("meme signe",value1,value2,value3)
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                #print(average,average2)
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                #print(model3.intercept_,model3.coef_)
                                for years in range(first_year-1,max(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(item).get("begin"))-1,-1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                df.to_csv('itemland_use_regression.csv',index = False) 
                                
                                    
                            else :
                                #print("signes differents")
                                for years in range(first_year-1,max(parameters.get("exeptions").get(item).get("begin"),parameters.get("exeptions").get(code).get("begin"))-1,-1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #print(df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year-1)]])
                                
                                df.to_csv('itemland_use_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    #print(years)
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    #print(value1,value2,value3)
                                    average=(value1+value2+value3)/3
                                    #print(average)
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('itemland_use_regression.csv',index = False) 
                            #else ("SI IL MAQUE DES DONNES A LA FIN"):
                             #   print("A FAIRE)
                        #break       