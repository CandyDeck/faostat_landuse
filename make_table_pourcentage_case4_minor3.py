import numpy as np

def make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust):
    for years in relevant_years_adjust:
            value_major = df.loc[((df['Item Code']==key)&(df['ISO3']==code)),[years]]
            if not value_major.isnull().values.all(): 
                value_major = float(value_major.to_string(index=False, header=False))
                if diagram.get(key).get("minor1") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    if not (df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                        value_minor1 = df.loc[((df['Item Code']==diagram.get(key).get("minor1"))&(df['ISO3']==code)),[years]]
                        value_minor1 = float(value_minor1.to_string(index=False, header=False))
                        p_minor1=value_minor1*100/value_major
                        dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),[years]]=p_minor1       
                        
                if diagram.get(key).get("minor2") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    if not (df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                        value_minor2 = df.loc[((df['Item Code']==diagram.get(key).get("minor2"))&(df['ISO3']==code)),[years]]
                        value_minor2 = float(value_minor2.to_string(index=False, header=False))
                        p_minor2=value_minor2*100/value_major
                        dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor2")),[years]]=p_minor2
            
                if diagram.get(key).get("minor3") in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    if not (df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]].isnull().values.all()):
                        value_minor3 = df.loc[((df['Item Code']==diagram.get(key).get("minor3"))&(df['ISO3']==code)),[years]]
                        value_minor3 = float(value_minor3.to_string(index=False, header=False))
                        p_minor3=value_minor3*100/value_major
                        dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),[years]]=p_minor3

    
    col_years = [col for col in dfs[key].columns if  col.startswith("Y")] 
    dfs[key] = dfs[key].replace(0, np.NaN)
    

    dfs[key]['min_value'] = dfs[key][col_years].min(axis=1 )
    dfs[key]['max_value'] = dfs[key][col_years].max(axis=1)
    dfs[key]['mean_value'] = dfs[key][col_years].mean(axis = 1, skipna = True)
    dfs[key]['std_dev'] = dfs[key][col_years].std(axis = 1, skipna = True)
    dfs[key]['var'] = dfs[key][col_years].var(axis = 1, skipna = True) 


    for code in country: 
        if not  (dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor1"))&(dfs[key]['ISO3']==code)),['min_value']].isnull().values.all()) :
            mini = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor1"))&(dfs[key]['ISO3']==code)),['min_value']]
            mini = float(mini.to_string(index=False, header=False))
            
            maxi = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor1"))&(dfs[key]['ISO3']==code)),['max_value']]
            maxi = float(maxi.to_string(index=False, header=False))

            if mini == maxi :
                dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor1")),['std_dev']]=0
        
        if not  (dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor2"))&(dfs[key]['ISO3']==code)),['min_value']].isnull().values.all()) :

            mini2 = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor2"))&(dfs[key]['ISO3']==code)),['min_value']]
            mini2 = float(mini2.to_string(index=False, header=False))
            
            maxi2 = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor2"))&(dfs[key]['ISO3']==code)),['max_value']]
            maxi2 = float(maxi2.to_string(index=False, header=False))
        
            if mini2 == maxi2 :
                dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor2")),['std_dev']]=0
        
        if not  (dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor3"))&(dfs[key]['ISO3']==code)),['min_value']].isnull().values.all()) :

            mini3 = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor3"))&(dfs[key]['ISO3']==code)),['min_value']]
            mini3 = float(mini3.to_string(index=False, header=False))
            
            maxi3 = dfs[key].loc[((dfs[key]['item']==diagram.get(key).get("minor3"))&(dfs[key]['ISO3']==code)),['max_value']]
            maxi3 = float(maxi3.to_string(index=False, header=False))

            if mini3 == maxi3 :
                dfs[key].loc[(dfs[key]['ISO3']==code)&(dfs[key]['item']==diagram.get(key).get("minor3")),['std_dev']]=0    
            
    return dfs
    
