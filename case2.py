import yaml
import calcul_1 as cal1
import calcul_2 as cal2
import adjustment as adj
import calcul_3_case1 as cal3c1
import calcul_3_case2 as cal3c2
import calcul_3_case3 as cal3c3
import make_table_pourcentage_case2_minor1 as mtpc2m1
import make_table_pourcentage_case2_minor2 as mtpc2m2
import make_table_pourcentage_case2_minor3 as mtpc2m3
import empty_cells_0706 as ec0706 
import calcul_pourcent_case2_minor3 as cpc2m3
import calcul_pourcent_case2_minor2 as cpc2m2
import calcul_pourcent_case2_minor1 as cpc2m1


with open(r'parameters.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)
    
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)

def solve(df, dfs,code,relevant_years, diagram,key,country,missing,year3b,year3e,year2e,year2b,year1e,year1b,a):
    
    if year3b and (min(a)==year3b):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year3b,year3e+1))]
        for years in relevant_years_adjust :             
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)

        relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
        for years in relevant_years_adjust2 :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c3.calculation(df,code,key,years)
        mtpc2m3.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust,relevant_years_adjust2)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False) 
    
    if year1b and (min(a)==year1b):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year1b,year1e+1))]
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)     
        
        relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
        for years in relevant_years_adjust2 :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c1.calculation(df,code,key,years)
        mtpc2m1.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust,relevant_years_adjust2)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False) 
  
    if year2b and (min(a)==year2b):
        relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year2b,year2e+1))]
        for years in relevant_years_adjust :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal1.calculation(df,code,key,years)
                cal2.calculation(df,code,key,years)
               
        relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
        for years in relevant_years_adjust2 :
            if key in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                cal3c2.calculation(df,code,key,years)                
        mtpc2m2.make_table(df, dfs,code,years, diagram,key,country,relevant_years_adjust,relevant_years_adjust2)
        file_name = str(key)+".csv"  #Change the column name accordingly
        dfs[key].to_csv(file_name, index=False)                 
    df.to_csv('land_use.csv',index = False)
    
    
    missing = ec0706.check(df, code, years,key,missing)
    #file_name = str(key)+".csv"  #Change the column name accordingly
    #dfs[key].to_csv(file_name, index=False) 
            
    if missing == 1 :
        if diagram.get(key).get("minor3") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year3b,year3e+1))]
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
            cpc2m3.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
        if diagram.get(key).get("minor2") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year2b,year2e+1))]
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
            cpc2m2.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)
        if diagram.get(key).get("minor1") in parameters.get("exeptions"):
            relevant_years_adjust = [make_valid_fao_year(year) for year in list(range(year1b,year1e+1))]                    
            relevant_years_adjust2 = [make_valid_fao_year(year) for year in range(parameters.get("year_of_interest").get("begin"),max(a))]
            cpc2m1.pourcentage(df, dfs,code,years, diagram,key,country,relevant_years_adjust2,relevant_years_adjust)
            adj.adjust(df, dfs,code,years, diagram,key,country)
        df.to_csv('land_use.csv',index = False)

    return df,dfs
 