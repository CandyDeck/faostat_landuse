from pathlib import Path

#import download_files
import numpy as np
import pandas as pd
import yaml
import fill_cells
import regression_crop as reg

with open(r'parameters_crop.yaml') as file:
    parameters = yaml.load(file, Loader=yaml.FullLoader)

with open(r'FAO_crop.yaml') as file:
    FAO_crop = yaml.load(file, Loader=yaml.FullLoader)  
def make_valid_fao_year(year):
    """Make a valid fao year string from int year"""
    return "Y" + str(year)


relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]


storage_root = Path("./land_use").absolute()
download_path = storage_root / "download"
data_path = storage_root / "data"



df_iso3 = pd.read_csv('../land_use/data/refreshed_production_crop.csv', encoding="latin-1") 
list_ISO3 = list(df_iso3['ISO3'])
country = [] 
for i in list_ISO3: 
    if i not in country: 
        country.append(i)
     
df = pd.read_csv('../land_use/data/refreshed_production_crop.csv', encoding="latin-1") 

col_years = [col for col in df.columns if  col.startswith("Y")] 
  
meta_col = ["ISO3", "Item Code", "Item","Unit"] 
        
df=df[meta_col + relevant_years]
df = df[df['Unit'] != 'million tonnes']
df = df[df['ISO3'] != 'not found']

df=df[df.ISO3.isin(list_ISO3)]
df.isnull().sum().sum()

for code in country :

    if not code in parameters.get("exeptions"):
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
    else:
        relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]

for code in country :
    print(code)
    for item in FAO_crop:
        if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
            fill_cells.fill(code, item, relevant_years,df,parameters)
           


for code in country :
    reg.regression(code,parameters,df,FAO_crop, col_years)
df.to_csv('crop_regression.csv',index = False)